"""Initialisation de l'API Flask"""

from flask import Flask
from flask_jwt_extended import JWTManager
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_uploads import UploadSet, IMAGES, configure_uploads
from flask_mail import Mail

from config import Config

db = SQLAlchemy()
migrate = Migrate()
jwt = JWTManager()
photos = UploadSet('photos', IMAGES)
mail = Mail()


def create_api(config_class=Config):
    """Configure and create the Flask app object

    Args:
        config_class: The configuration class of the app
    """
    app = Flask(__name__)
    app.config.from_object(config_class)

    db.init_app(app)
    migrate.init_app(app, db)
    jwt.init_app(app)
    configure_uploads(app, photos)
    mail.init_app(app)

    from app.main import bp as main_bp
    app.register_blueprint(main_bp, url_prefix='/')

    from app.auth import bp as auth_bp
    app.register_blueprint(auth_bp, url_prefix='/api/auth')

    from app.article import bp as article_bp
    app.register_blueprint(article_bp, url_prefix='/api/article')

    from app.message import bp as message_bp
    app.register_blueprint(message_bp, url_prefix='/api/message')

    from app.user import bp as user_bp
    app.register_blueprint(user_bp, url_prefix='/api/user')

    @app.errorhandler(404)
    def page_not_found(e):
        return """
        <body style='background-color: black;color: white'>
            <h1>Looks like you're lost</h1>
            <img src='https://http.cat/404'>
        </body>
        """, 404

    return app
