"""Article related endpoints"""
import datetime

from flask import current_app, request, send_file, redirect
from flask_restful import Resource, reqparse
from flask_jwt_extended.config import config
from flask_jwt_extended import get_jwt_identity, jwt_required, create_access_token, decode_token
import uuid

from jwt import ExpiredSignatureError
from sqlalchemy import or_

from app import db, photos
from app.models import User, Article, Photo, Purchase
from app.utils import parse_json


class GetPurchaseToken(Resource):
    @jwt_required
    def post(self):
        user = User.query.filter_by(pseudo=get_jwt_identity()).first()
        token = create_access_token(identity=user.pseudo, expires_delta=datetime.timedelta(hours=1))

        return {'token': token}, 200

class PurchaseArticle(Resource):
    def get(self, article_id, user_token):
        try:
            token = decode_token(encoded_token=user_token)
        except ExpiredSignatureError as e:
            return {'msg': 'bad token'.format(e.__repr__())}, 500
        except Exception as e:
            return {'msg': e.__repr__()}, 500

        user_pseudo = token.get(config.identity_claim_key, None)
        if not user_pseudo:
            return {'msg': 'bad token'}, 500

        user = User.query.filter_by(pseudo=user_pseudo).first()
        article = Article.query.filter_by(id=article_id).first()

        purchase = Purchase(buyer=user, article=article)

        db.session.add(purchase)
        try:
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            db.session.close()
            return {'msg': e.__repr__()}, 500

        return {'msg': 'purchase register'}, 200


class Upload(Resource):
    """upload endpoint definition"""
    @jwt_required
    def post(self):
        """Endpoint used to upload a new article. This endpoint required a valid access token.

        Args:
            name: The name of the article
            price: The price of the article
            description: An optional description of the article..

        Returns:
            500: {'msg': ``error``} -> if there is a issue with the database's transaction
            200: {'msg': 'new article upload'} -> if everything worked well

        """
        print(list(request.files.keys()))
        print(request.form)

        # args = parse_json(['name', 'price', 'description', 'image'])
        parser = reqparse.RequestParser()
        for arg in ['name', 'price', 'description']:
            parser.add_argument(arg, location='form', required=True)

        args = parser.parse_args()

        author = User.query.filter_by(pseudo=get_jwt_identity()).first()

        print(args['price'])
        new_article = Article(name=args['name'], price=float(args['price']), author=author)

        if args['description']:
            new_article.description = args['description']

        img_uuid = str(uuid.uuid1())
        filename = photos.save(request.files['image'], name=f"{img_uuid}.")
        print(filename)

        img_extention = filename.split('.')[1]
        print(img_extention)
        new_photo = Photo(uuid=img_uuid, extension=img_extention, article=new_article)

        db.session.add_all([new_article, new_photo])
        try:
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            db.session.close()
            return {'msg': e.__repr__()}, 500

        return {'msg': 'new article upload', 'article': new_article.serialize}, 200


class Delete(Resource):
    @jwt_required
    def post(self):
        args = parse_json(['id'])
        if not args['id']:
            return {'msg': 'error not article id specified'}, 400

        user = User.query.filter_by(pseudo=get_jwt_identity()).first()
        article = Article.query.filter_by(id=int(args['id'])).first()

        if not article or article.deleted:
            return {'msg': 'the article doesn\'t exist'}, 400

        if article.author != user:
            return {'msg': 'error you\'re not the author of this article'}, 400

        article.deleted = True

        try:
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            db.session.close()
            return {'msg': e.__repr__()}, 500

        return {'msg': 'article deleted'}, 200


class Get(Resource):
    """get endpoint definition"""
    def post(self):
        """Endpoint used to retrieve a list of articles.

        Args:
            page: The page queried (begin at 1)
            nb_article_per_page: An optional argument operating the default value

        Returns:
            200: {'articles': {list of articles}, 'next_page': ``next_page``,
                  'prev_page': ``prev_page``} -> if everything worked well

            If there is no next or previous page the corresponding argument will be None.
            If there is there is no article for this page 'articles' will be an empty list
        """
        args = parse_json(['page'], optional_args=['nb_article_per_page'])
        if args['nb_article_per_page']:
            nb_article_per_page = int(args['nb_article_per_page'])
        else:
            nb_article_per_page = current_app.config['DEFAULT_NB_ARTICLES_PER_PAGE']

        articles = Article.query.filter(or_(Article.deleted==False, Article.deleted==None)).order_by(Article.create_at.desc())\
                          .paginate(int(args['page']), nb_article_per_page, False)

        next_page = articles.next_num if articles.has_next else None
        prev_page = articles.prev_num if articles.has_prev else None

        return {'articles': [article.serialize for article in articles.items],
                'next_page': next_page, 'prev_page': prev_page}, 200

class GetPurchases(Resource):
    @jwt_required
    def post(self):

        args = parse_json(['page'], optional_args=['nb_purchase_per_page'])
        if args['nb_purchase_per_page']:
            nb_purchase_per_page = int(args['nb_purchase_per_page'])
        else:
            nb_purchase_per_page = current_app.config['DEFAULT_NB_PURCHASE_PER_PAGE']

        user = User.query.filter_by(pseudo=get_jwt_identity()).first()

        print(user)

        purchases = Purchase.query.join(Article, Article.id == Purchase.article_id)\
                            .filter(Article.author == user).order_by(Purchase.purchase_at.desc())\
                            .paginate(int(args['page']), nb_purchase_per_page, False)

        next_page = purchases.next_num if purchases.has_next else None
        prev_page = purchases.prev_num if purchases.has_prev else None

        return {'purchases': [purchase.serialize for purchase in purchases.items],
                'next_page': next_page, 'prev_page': prev_page}, 200


class Image(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        for arg in ['id']:
            parser.add_argument(arg, required=True)

        args = parser.parse_args()

        article = Article.query.filter_by(id=int(args['id'])).first()
        img = Photo.query.filter_by(article=article).first()

        return send_file(img.link)
