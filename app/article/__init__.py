"""Article blueprint"""

from flask import Blueprint
from flask_restful import Api
from app.article.routes import *

bp = Blueprint('article', __name__)
api = Api(bp)

api.add_resource(Upload, '/upload')

api.add_resource(Get, '/get')

api.add_resource(Image, '/image')

api.add_resource(Delete, '/delete')

api.add_resource(GetPurchaseToken, '/getpurchasetoken')

api.add_resource(PurchaseArticle, '/purchase/<article_id>/<user_token>')

api.add_resource(GetPurchases, '/getpurchases')