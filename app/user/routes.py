"""User related endpoints"""

from flask_jwt_extended import (jwt_required, get_jwt_identity)
from flask_restful import Resource

from app import db
from app.models import User
from app.utils import parse_json


class userInfo(Resource):
    @jwt_required
    def post(self):
        user = User.query.filter_by(pseudo=get_jwt_identity()).first()

        if not user:
            return {'msg': 'user not found'}, 500

        return {'user': user.serialize}, 200


class updateUserInfo(Resource):
    @jwt_required
    def post(self):
        args = parse_json(['pseudo', 'email', 'public_key'])

        user = User.query.filter_by(pseudo=get_jwt_identity()).first()

        user.pseudo = args['pseudo']
        user.email = args['email']
        user.public_key = args['public_key']

        try:
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            db.session.close()
            return {'msg': e.__repr__()}, 500

        return {'msg': 'user info updated'}, 200

