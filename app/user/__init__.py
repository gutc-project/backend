"""User blueprint"""

from flask import Blueprint
from flask_restful import Api
from app.user.routes import *

bp = Blueprint('user', __name__)
api = Api(bp)

api.add_resource(userInfo, '/getuserinfo')

api.add_resource(updateUserInfo, '/updateuserinfo')
