"""Message blueprint"""

from flask import Blueprint
from flask_restful import Api
from app.message.routes import *

bp = Blueprint('message', __name__)
api = Api(bp)

api.add_resource(SendMail, '/sendmail')
