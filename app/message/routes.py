"""Messaging related endpoints"""

from flask_restful import Resource
from flask_jwt_extended import get_jwt_identity, jwt_required
from flask_mail import Message

from app import db, mail
from app.models import User, Article
from app.utils import parse_json


class SendMail(Resource):
    @jwt_required
    def post(self):
        args = parse_json(['text_body', 'article_id'])

        article = Article.query.filter_by(id=int(args['article_id'])).first()
        sender = User.query.filter_by(pseudo=get_jwt_identity()).first()
        recipient = article.author

        if not article or not sender or not recipient:
            return {'msg': 'error information incorrect'}, 400

        msg = Message(subject="[UTChange] Quelqu'un est intéressé par votre article: {}".format(article.name),
                      sender='noreply@utchange.picasoft.net', recipients=[recipient.email])
        msg.html = "<div style=\"white-space: pre\">Message de {}:<br> {}</div>".format(sender.email, args['text_body'])

        try:
            mail.send(msg)
        except Exception as e:
            return {'msg': e.__repr__()}, 500

        return {'msg': 'mail envoyé'}, 200
