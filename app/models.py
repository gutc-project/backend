"""This file contains the description of the database used by SQLAlchemy"""

import os
from flask import current_app
from datetime import datetime
from werkzeug.security import check_password_hash, generate_password_hash

from app import db, jwt
from app.utils import dump_datetime


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    """jwt utils fonction used to check if a token is blacklisted.

    *By default every tokens are blacklisted*.
    When a token (access our refresh) is generated for the user we whitelist these particular tokens.

    Args:
        decrypted_token: A decrypted token to check

    Returns:
        True if the token is blacklisted else False.
    """
    jti = decrypted_token['jti']
    token_type = decrypted_token['type']
    sub = decrypted_token['sub']

    user = User.query.filter_by(pseudo=sub).first()

    if user is not None:
        if token_type == "access":
            return user.access_token != jti

        elif token_type == "refresh":
            return user.refresh_token != jti

    return True


class User(db.Model):
    """The User table"""
    id = db.Column(db.Integer, primary_key=True)

    pseudo = db.Column(db.String(50), unique=True, nullable=False, index=True)
    email = db.Column(db.String(100), unique=True, nullable=False)

    public_key = db.Column(db.String(200))

    password_hash = db.Column(db.String(128), nullable=False)

    access_token = db.Column(db.String(40))
    refresh_token = db.Column(db.String(40))

    create_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    last_connection = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    articles = db.relationship('Article', backref='author', lazy='dynamic')
    purchases = db.relationship('Purchase', backref='buyer', lazy='dynamic')

    def __repr__(self):
        return f"<User {self.id} {self.pseudo} {self.email} {self.last_connection}>"

    def set_password(self, password):
        """hash and set the password"""
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        """check if the password if correct"""
        return check_password_hash(self.password_hash, password)

    @property
    def serialize(self):
        """Return a jsonable representation of the user"""
        return {
            'id': self.id,
            'pseudo': self.pseudo,
            'email': self.email,
            'public_key': self.public_key or 'none',
            'last_connection': dump_datetime(self.last_connection)
        }


class Article(db.Model):
    """The Article table"""
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(150), nullable=False, index=True)
    price = db.Column(db.Float, nullable=False, index=True)
    description = db.Column(db.Text)
    location = db.Column(db.String(150))
    create_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

    photos = db.relationship('Photo', backref='article', lazy='dynamic')
    purchases = db.relationship('Purchase', backref='article', lazy='dynamic')

    deleted = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return f"<Article {self.id} {self.name} {self.price} {self.author}>"

    @property
    def serialize(self):
        """Return a jsonable representation of the article"""
        return {
            'id': self.id,
            'name': self.name,
            'price': self.price,
            'description': self.description,
            'create_at': dump_datetime(self.create_at),
            'author': self.author.serialize
        }


class Purchase(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    article_id = db.Column(db.Integer, db.ForeignKey('article.id'))
    buyer_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    purchase_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    @property
    def serialize(self):
        """Return a jsonable representation of the purchase"""
        return {
            'id': self.id,
            'buyer': self.buyer.serialize,
            'article': self.article.serialize,
            'purchase_at': dump_datetime(self.purchase_at)
        }


class Photo(db.Model):
    """The Photo table"""
    uuid = db.Column(db.String(36), primary_key=True)

    extension = db.Column(db.String(36), nullable=False)
    create_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    article_id = db.Column(db.Integer, db.ForeignKey('article.id'), nullable=False)

    def __repr__(self):
        return f"<Photo {self.uuid} {self.extension} {self.create_at} {self.article}>"

    @property
    def link(self):
        """Return the link to the photo file"""
        return os.path.join(current_app.config['UPLOADED_PHOTOS_DEST'], f"{self.uuid}.{self.extension}")
