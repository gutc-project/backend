"""Authentication related endpoints"""

from flask_restful import Resource
from flask_jwt_extended import (create_access_token, create_refresh_token,
                                get_jwt_identity, jwt_refresh_token_required, decode_token)

from app import db
from app.models import User
from app.utils import parse_json


def pseudo_ok(pseudo):
    """check if the pseudo is not already used"""
    return User.query.filter_by(pseudo=pseudo).first() is None


def email_ok(email):
    """check if the email is not already used"""
    return User.query.filter_by(email=email).first() is None


class isPseudoAvailable(Resource):
    """isPseudoAvailable endpoint definition"""
    def post(self):
        """This endpoint takes a pseudo and check if the pseudo is available

        Returns:
            200: {'pseudo': 'already used'} -> if the pseudo if not ok
            202: {'pseudo': 'available'} -> if the pseudo is ok
        """
        args = parse_json(['pseudo'])
        print(not pseudo_ok(args['pseudo']))
        if not pseudo_ok(args['pseudo']):
            return {'pseudo': 'already used'}, 200
        return {'pseudo': 'available'}, 202


class isEmailAvailable(Resource):
    """isEmailAvailable endpoint definition"""
    def post(self):
        """This endpoint takes a email and check if the pseudo is available

        Returns:
            200: {'email': 'already used'} -> if the email if not ok
            202: {'email': 'available'} -> if the email is ok
        """
        args = parse_json(['email'])
        if not email_ok(args['email']):
            return {'email': 'already used'}, 200
        return {'email': 'available'}, 202


class SignUp(Resource):
    """signup endpoint definition"""
    def post(self):
        """Endpoint used to Sign up a new user

        Args:
            pseudo: The pseudo of the user. Must be unique.
            email: The email of the user. Must be unique.
            password: The password of the user

        Returns:
            400: {'msg': 'invalide pseudo'} -> if the pseudo is not ok
            400: {'msg': 'invalide email'} -> if the email if not ok
            500: {'msg': ``error``} -> if there is a issue with the database's transaction
            201: {'msg': 'user add'} ->  if everything worked
        """
        args = parse_json(['pseudo', 'email', 'password'])

        if not pseudo_ok(args['pseudo']):
            return {'msg': 'invalide pseudo'}, 400

        if not email_ok(args['email']):
            return {'msg': 'invalide email'}, 400

        user = User(pseudo=args['pseudo'], email=args['email'])
        user.set_password(args['password'])
        db.session.add(user)
        try:
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            db.session.close()
            return {'msg': e.__repr__()}, 500

        return {'msg': 'user add'}, 201


class SignIn(Resource):
    """signin endpoint definition"""
    def post(self):
        """Endpoint used to Sign in a user.
        This endpoint creates and reruns a new access and refresh token for the user.

        Args:
            pseudo: the pseudo of the user
            password: the password of the user

        Returns:
            400: {'msg': 'invalide pseudo'} -> if the pseudo is not ok
            400: {'msg': 'invalide password'} -> if the password is not ok
            200: {'msg': 'login successful', 'access_token': ``access_token``,
                'refresh_token': ``refresh_token``} -> if everything worked well
        """
        args = parse_json(['pseudo', 'password'])

        user = User.query.filter_by(pseudo=args['pseudo']).first()

        if user is None:
            return {'msg': 'invalide pseudo'}, 400

        if not user.check_password(args['password']):
            return {'msg': 'invalide password'}, 400

        access_token = create_access_token(identity=args['pseudo'], fresh=True)
        refresh_token = create_refresh_token(identity=args['pseudo'])

        user.access_token = decode_token(access_token)['jti']
        user.refresh_token = decode_token(refresh_token)['jti']

        try:
            db.session.commit()
            db.session.close()
        except Exception as e:
            db.session.rollback()
            db.session.close()
            return {'msg': e.__repr__()}, 500

        return {'msg': 'login successful', 'access_token': access_token,
                'refresh_token': refresh_token}, 200

class Refresh(Resource):
    """refresh endpoint definition"""
    @jwt_refresh_token_required
    def post(self):
        """Endpoint used to get a new access token if the refresh token is ok.

        Returns:
            422: {'msg': 'Bad token'} -> if the refresh token if not ok
            500: {'msg': ``error``} -> if there is a issue with the database's transaction
            200: {'msg': 'new access token created', 'access_token': ``new_token``} -> if everything worked well
        """
        current_user = get_jwt_identity()
        new_token = create_access_token(identity=current_user)

        user = User.query.filter_by(pseudo=current_user).first()

        if user is None:
            return {'msg': 'Bad token'}, 422

        user.access_token = decode_token(new_token)['jti']

        try:
            db.session.commit()
            db.session.close()
        except Exception as e:
            db.session.rollback()
            db.session.close()
            return {'msg': e.__repr__()}, 500

        return {'msg': 'new access token created', 'access_token': new_token}, 200
