"""Authentication blueprint"""

from flask import Blueprint
from flask_restful import Api
from app.auth.routes import *


bp = Blueprint('auth', __name__)
api = Api(bp)

api.add_resource(isPseudoAvailable, '/ispseudoavailable')
api.add_resource(isEmailAvailable, '/isemailavailable')

api.add_resource(SignUp, '/signup')

api.add_resource(SignIn, '/signin')
api.add_resource(Refresh, '/refresh')
