"""Endpoint for serving the SPA"""

from flask_restful import Resource
from flask import send_from_directory, current_app


class SPA(Resource):
    def get(self):
        print('SPA')
        return send_from_directory(current_app.config['STATIC_SPA_FOLDER'], 'index.html')


class SPAResource(Resource):
    def get(self, path):
        print('SPAResource', path)
        if not path:
            print("aieaieaiz")
        return send_from_directory(current_app.config['STATIC_SPA_FOLDER'], path)
