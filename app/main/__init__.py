"""Main blueprint"""

from flask import Blueprint
from flask_restful import Api
from app.main.routes import *

bp = Blueprint('main', __name__)
api = Api(bp)

api.add_resource(SPA, '')
api.add_resource(SPAResource, '<path:path>')
