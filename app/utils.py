"""Utility functions used by the Flask app"""

from flask_restful import reqparse


def parse_json(required_args, optional_args=None):
    """Parse the json input of a post request.

    Args:
        required_args: a list of required arguments
        optional_args: a list of optional arguments

    Returns:
        A dict of the argument names and values
    """
    parser = reqparse.RequestParser()
    for arg in required_args:
        parser.add_argument(arg, location='json', type=str, required=True)
    if optional_args:
        for arg in optional_args:
            parser.add_argument(arg, location='json', type=str, required=False)
    return parser.parse_args()


def dump_datetime(value):
    """Deserialize datetime object into string form for JSON processing."""
    if value is None:
        return None
    return value.strftime("%Y-%m-%dT%H:%M:%S")
