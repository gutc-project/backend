"""Configuration de l'API Flask"""

import os
from dotenv import load_dotenv
from datetime import timedelta

basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))


class Config(object):
    """Classe regroupant les paramètres de configurations de Flask"""
    # Global conf
    SECRET_KEY = os.environ.get('G1_SECRET_KEY') or "P455W0RD"
    DEFAULT_NB_ARTICLES_PER_PAGE = os.environ.get('G1_DEFAULT_NB_ARTICLES_PER_PAGE') or 10
    DEFAULT_NB_PURCHASE_PER_PAGE = os.environ.get('G1_DEFAULT_NB_PURCHASE_PER_PAGE') or 10

    # SQLAlchemy conf
    SQLALCHEMY_DATABASE_URI = os.environ.get('G1_DATABASE_URL') or \
                              'sqlite:///' + os.path.join(basedir, 'backend.sqlite')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # JWT conf
    JWT_SECRET_KEY = os.environ.get('G1_JWT_SECRET_KEY') or SECRET_KEY
    if os.environ.get('G1_JWT_ACCESS_TOKEN_EXPIRES'):
        JWT_ACCESS_TOKEN_EXPIRES = int(os.environ.get('G1_JWT_ACCESS_TOKEN_EXPIRES'))
    else:
        JWT_ACCESS_TOKEN_EXPIRES = timedelta(minutes=15)
    if os.environ.get('G1_JWT_REFRESH_TOKEN_EXPIRES'):
        JWT_ACCESS_TOKEN_EXPIRES = int(os.environ.get('G1_JWT_REFRESH_TOKEN_EXPIRES'))
    else:
        JWT_ACCESS_TOKEN_EXPIRES = timedelta(days=30)
    JWT_IDENTITY_CLAIM = 'sub'
    JWT_BLACKLIST_ENABLED = True

    # Photo upload conf
    ALLOWED_EXTENSIONS = ['jpeg', 'jpg', 'png']
    UPLOADED_PHOTOS_DEST = os.environ.get('G1_UPLOADED_PHOTOS_DEST') or \
                           os.path.join(basedir, 'static', 'photos')

    # Mail
    MAIL_SERVER = os.environ.get('G1_MAIL_SERVER') or 'localhost'
    MAIL_PORT = int(os.environ.get('G1_MAIL_PORT') or 8025)
    MAIL_USE_TLS = os.environ.get('G1_MAIL_USE_TLS') is not None

    # SPA
    STATIC_SPA_FOLDER = os.environ.get('G1_STATIC_SPA_FOLDER') or \
                        os.path.join(basedir, 'static', 'dist', 'Gune-frontend')
