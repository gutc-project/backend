"""Point d'entrée de l'API"""

from app import create_api, db
from app.models import User, Article, Photo

app = create_api()


@app.shell_context_processor
def make_shell_context():
    """Définition des variables utilisables dans le shell FLask"""
    return {'db': db, 'User': User,
            'Article': Article, 'Photo': Photo
            }
