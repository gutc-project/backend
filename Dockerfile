FROM python:3.8-alpine

RUN adduser -D utchanger

WORKDIR /home/utchanger

RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev

COPY requirements.txt requirements.txt
RUN python -m venv venv
RUN venv/bin/pip install -r requirements.txt

RUN venv/bin/pip install gunicorn==19.9.0

RUN mkdir -p /var/www/utchange/static
RUN chown -R utchanger:utchanger /var/www/utchange

COPY app app
COPY migrations migrations
COPY backend.py config.py boot.sh ./
RUN chmod +x boot.sh

ENV FLASK_APP backend.py

RUN chown -R utchanger:utchanger ./
USER utchanger

EXPOSE 5555
ENTRYPOINT ["./boot.sh"]
