"""tests for the sending of messages """

import json
from io import BytesIO

import pytest
from datetime import datetime
from unittest.mock import MagicMock

from config import Config
from app.models import Article
from tests.utils import get_mocked_client
from app import mail


class TestConfig(Config):
    """Testing configuration"""
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'


client = get_mocked_client(TestConfig)

# testing data
toto_data = {'pseudo': 'tototo', 'email': 'toto@example.fr',
             'password': 'test'}
# testing data


class TestEmail:
    def test_get_user_info(self, client):
        # Sign up
        rv = client.post('/api/auth/signup', data=json.dumps(toto_data), content_type='application/json')
        assert rv.status_code == 201 and rv.get_json() == {'msg': 'user add'}

        # Sign in
        rv = client.post('/api/auth/signin',
                         data=json.dumps({'pseudo': toto_data['pseudo'], 'password': toto_data['password']}),
                         content_type='application/json')
        assert rv.status_code == 200 and rv.get_json()['msg'] == 'login successful' and \
               rv.get_json()['access_token'] and rv.get_json()['refresh_token']
        access_token = rv.get_json()['access_token']
        headers = {'Authorization': f"Bearer {access_token}"}

        # get user info
        rv = client.post('/api/user/getuserinfo', headers=headers)
        assert rv.status_code == 200
        user = rv.get_json()['user']
        assert user['id'] == 1
        assert user['pseudo'] == toto_data['pseudo']
        assert user['email'] == toto_data['email']

    def test_update_user_info(self, client):
        # Sign in
        rv = client.post('/api/auth/signin',
                         data=json.dumps({'pseudo': toto_data['pseudo'], 'password': toto_data['password']}),
                         content_type='application/json')
        assert rv.status_code == 200 and rv.get_json()['msg'] == 'login successful' and \
               rv.get_json()['access_token'] and rv.get_json()['refresh_token']
        access_token = rv.get_json()['access_token']
        headers = {'Authorization': f"Bearer {access_token}"}

        # update user info
        rv = client.post('/api/user/updateuserinfo', data=json.dumps({'pseudo': 'tototo', 'email': 'tata@example.fr',
                                                                      'public_key': '12345'}),
                         content_type='application/json', headers=headers)
        assert rv.status_code == 200 and rv.get_json()['msg'] == 'user info updated'

        # check user info
        rv = client.post('/api/user/getuserinfo', headers=headers)
        assert rv.status_code == 200
        user = rv.get_json()['user']
        assert user['id'] == 1
        assert user['pseudo'] == 'tototo'
        assert user['email'] == 'tata@example.fr'
        assert user['public_key'] == '12345'
