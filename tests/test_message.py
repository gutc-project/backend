"""tests for the sending of messages """

import json
from io import BytesIO

import pytest
from datetime import datetime
from unittest.mock import MagicMock

from config import Config
from app.models import Article
from tests.utils import get_mocked_client
from app import mail


class TestConfig(Config):
    """Testing configuration"""
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
    UPLOADED_PHOTOS_DEST = 'tests/tmp'


client = get_mocked_client(TestConfig)

# testing data
toto_data = {'pseudo': 'tototo', 'email': 'toto@example.fr',
             'password': 'test'}

tata_data = {'pseudo': 'tatata', 'email': 'tata@example.fr',
             'password': 'test'}

with open('tests/test_img.png', 'rb') as f:
    img = (BytesIO(f.read()), 'test_img.png')
article1_data = {"name": "my article n°1", "price": 3.1415,
                 "description": "a very long description",
                 "image": img}
# testing data


class TestEmail:
    def test_send_email(self, client, monkeypatch):
        # Sign up
        rv = client.post('/api/auth/signup', data=json.dumps(toto_data), content_type='application/json')
        assert rv.status_code == 201 and rv.get_json() == {'msg': 'user add'}
        rv = client.post('/api/auth/signup', data=json.dumps(tata_data), content_type='application/json')
        assert rv.status_code == 201 and rv.get_json() == {'msg': 'user add'}

        # Upload
        rv = client.post('/api/auth/signin',
                         data=json.dumps({'pseudo': toto_data['pseudo'], 'password': toto_data['password']}),
                         content_type='application/json')
        assert rv.status_code == 200 and rv.get_json()['msg'] == 'login successful' and \
               rv.get_json()['access_token'] and rv.get_json()['refresh_token']
        access_token = rv.get_json()['access_token']
        headers = {'Authorization': f"Bearer {access_token}"}
        rv = client.post('/api/article/upload', data=article1_data, headers=headers)
        assert rv.status_code == 200 and rv.get_json()['msg'] == 'new article upload'

        # Send mail
        rv = client.post('/api/auth/signin',
                         data=json.dumps({'pseudo': tata_data['pseudo'], 'password': tata_data['password']}),
                         content_type='application/json')
        assert rv.get_json()['msg'] == 'login successful' and rv.status_code == 200 and \
               rv.get_json()['access_token'] and rv.get_json()['refresh_token']
        access_token = rv.get_json()['access_token']
        headers = {'Authorization': f"Bearer {access_token}"}

        with mail.record_messages() as outbox:
            rv = client.post('/api/message/sendmail', data=json.dumps({'text_body': "hello\ncomment vas tu ?",
                                                                   'article_id': 1}),
                             content_type='application/json', headers=headers)
            assert rv.get_json() == {'msg': 'mail envoyé'} and rv.status_code == 200

            msg = outbox[0]
            assert msg.subject == "[UTChange] Quelqu'un est intéressé par votre article: my article n°1"
            assert msg.sender == "noreply@utchange.picasoft.net"
            assert msg.recipients == ["toto@example.fr"]
            assert msg.html == "<div style=\"white-space: pre\">Message de tata@example.fr:<br> hello\ncomment vas tu ?</div>"
