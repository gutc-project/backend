"""Utility fonctions for testing"""

import shutil

import pytest
from app import create_api, db


def get_mocked_client(testing_config):
    """Get a mock af the Flask app

    Args:
        testing_config: Testing configuration class inheriting from config.Config
    """
    @pytest.fixture(scope="class")
    def client():
        """Mock of the Flask app"""
        app = create_api(testing_config)

        with app.test_client() as client:
            context = app.app_context()
            context.push()
            db.create_all()
            print("SETUP")
            yield client

        shutil.rmtree('tests/tmp', ignore_errors=True)
        db.session.remove()
        db.drop_all()
        context.pop()
        print("TEAR DOWN")

    return client
