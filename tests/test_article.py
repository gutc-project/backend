"""tests for the upload and retrieval of articles"""

import json
from io import BytesIO

import pytest
from datetime import datetime

from config import Config
from app.models import Article
from tests.utils import get_mocked_client


class TestConfig(Config):
    """Testing configuration"""
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
    DEFAULT_NB_ARTICLES_PER_PAGE = 2
    UPLOADED_PHOTOS_DEST = 'tests/tmp'


client = get_mocked_client(TestConfig)

# testing data
toto_data = {'pseudo': 'tototo', 'email': 'toto@example.fr',
             'password': 'test'}

tata_data = {'pseudo': 'tatata', 'email': 'tata@example.fr',
             'password': 'test'}

with open('tests/test_img.png', 'rb') as f:
    img = (BytesIO(f.read()), 'test_img.png')
article1_data = {"name": "my article n°1", "price": 3.1415,
                 "description": "a very long description",
                 "image": img}
with open('tests/test_img.png', 'rb') as f:
    img = (BytesIO(f.read()), 'test_img.png')
article2_data = {"name": "my article n°2", "price": 42,
                 "description": "a very long description",
                 "image": img}
with open('tests/test_img.png', 'rb') as f:
    img = (BytesIO(f.read()), 'test_img.png')
article3_data = {"name": "my article n°3", "price": 2.7182,
                 "description": "a very long description",
                 "image": img}
# testing data


class TestArticles:
    """Tests for the upload and retrieval of articles"""
    def test_append_article(self, client):
        """Test the upload of an article"""
        # Sign up
        rv = client.post('/api/auth/signup', data=json.dumps(toto_data), content_type='application/json')
        assert rv.status_code == 201 and rv.get_json() == {'msg': 'user add'}
        rv = client.post('/api/auth/signin',
                         data=json.dumps({'pseudo': toto_data['pseudo'], 'password': toto_data['password']}),
                         content_type='application/json')
        assert rv.status_code == 200 and rv.get_json()['msg'] == 'login successful' and \
               rv.get_json()['access_token'] and rv.get_json()['refresh_token']

        # Getting the access token
        access_token = rv.get_json()['access_token']
        headers = {'Authorization': f"Bearer {access_token}"}

        # Uploads
        rv = client.post('/api/article/upload', data=article1_data, headers=headers)
        assert rv.status_code == 200 and rv.get_json()['msg'] == 'new article upload'

        rv = client.post('/api/article/upload', data=article2_data, headers=headers)
        assert rv.status_code == 200 and rv.get_json()['msg'] == 'new article upload'

        rv = client.post('/api/article/upload', data=article3_data, headers=headers)
        assert rv.status_code == 200 and rv.get_json()['msg'] == 'new article upload'

        article1 = Article.query.order_by(Article.create_at).first()
        assert article1_data["name"] == article1.name \
            and article1_data["price"] == article1.price \
            and article1.create_at.date() == datetime.utcnow().date()

    def test_get_articles(self, client):
        """Test the retrieval of articles"""
        rv = client.post('/api/article/get', data=json.dumps({'page': 1}),
                         content_type='application/json')
        assert rv.status_code == 200
        articles = rv.get_json()['articles']
        assert len(articles) == 2
        assert not rv.get_json()['prev_page']\
            and rv.get_json()['next_page'] == 2

        assert articles[0]['name'] == article3_data['name']\
            and articles[1]['name'] == article2_data['name']\

        rv = client.post('/api/article/get', data=json.dumps({'page': 2}),
                         content_type='application/json')
        articles = rv.get_json()['articles']
        assert rv.status_code == 200
        assert len(articles) == 1
        assert rv.get_json()['prev_page'] == 1 \
               and rv.get_json()['next_page'] == None

        assert articles[0]['name'] == article1_data['name']

        rv = client.post('/api/article/get', data=json.dumps({'page': 3}),
                         content_type='application/json')
        assert rv.status_code == 200
        articles = rv.get_json()['articles']
        assert len(articles) == 0
        assert rv.get_json()['prev_page'] == 2 \
               and rv.get_json()['next_page'] == None

    def test_delete_articles(self, client):
        # test author ok
        rv = client.post('/api/auth/signin',
                         data=json.dumps({'pseudo': toto_data['pseudo'], 'password': toto_data['password']}),
                         content_type='application/json')
        assert rv.status_code == 200 and rv.get_json()['msg'] == 'login successful' and \
               rv.get_json()['access_token'] and rv.get_json()['refresh_token']
        access_token = rv.get_json()['access_token']
        headers = {'Authorization': f"Bearer {access_token}"}

        rv = client.post('/api/article/delete', data=json.dumps({'id': 1}),
                         content_type='application/json', headers=headers)
        assert rv.status_code == 200 and rv.get_json()['msg'] == 'article deleted'

        # test article nok
        rv = client.post('/api/article/delete', data=json.dumps({'id': 1}),
                         content_type='application/json', headers=headers)
        assert rv.status_code == 400 and rv.get_json()['msg'] == 'the article doesn\'t exist'

        # test author nok
        rv = client.post('/api/auth/signup', data=json.dumps(tata_data), content_type='application/json')
        assert rv.status_code == 201 and rv.get_json() == {'msg': 'user add'}
        rv = client.post('/api/auth/signin',
                         data=json.dumps({'pseudo': tata_data['pseudo'], 'password': tata_data['password']}),
                         content_type='application/json')
        assert rv.status_code == 200 and rv.get_json()['msg'] == 'login successful' and \
               rv.get_json()['access_token'] and rv.get_json()['refresh_token']
        access_token = rv.get_json()['access_token']
        headers = {'Authorization': f"Bearer {access_token}"}

        rv = client.post('/api/article/delete', data=json.dumps({'id': 2}),
                         content_type='application/json', headers=headers)
        assert rv.status_code == 400 and rv.get_json()['msg'] == 'error you\'re not the author of this article'
