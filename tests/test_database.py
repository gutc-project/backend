"""tests for the database"""

import pytest
from sqlalchemy.exc import IntegrityError

from app import db
from app.models import User, Article
from config import Config
from tests.utils import get_mocked_client


class TestConfig(Config):
    """Testing configuration"""
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'


client = get_mocked_client(TestConfig)


class TestUser:
    """Tests for the User model"""
    def test_append_user(self, client):
        """Test the insertion of a new User"""
        titi = User(pseudo='tutu', email='titi@example.fr')
        titi.set_password('test')
        toto = User(pseudo='tata', email='toto@example.fr')
        toto.set_password('123')
        db.session.add_all([titi, toto])
        db.session.commit()

        tete_same_pseudo = User(pseudo='tutu', email='tete@example.fr')
        tete_same_pseudo.set_password('h4ck3')
        db.session.add(tete_same_pseudo)
        with pytest.raises(IntegrityError):
            db.session.commit()
        db.session.rollback()

        tete_same_email = User(pseudo='tete', email='titi@example.fr')
        tete_same_email.set_password('h4ck3')
        db.session.add(tete_same_email)
        with pytest.raises(IntegrityError):
            db.session.commit()
        db.session.rollback()

        tete_same_first_and_last_name = User(pseudo='tete', email='tete@example.fr')
        tete_same_first_and_last_name.set_password('h4ck3')
        db.session.add(tete_same_first_and_last_name) # ok car first_name et last_name ne sont pas UNIQUE

        tyty_no_password = User(pseudo='toutou', email='toutou@example.fr')
        db.session.add(tyty_no_password)
        with pytest.raises(IntegrityError):
            db.session.commit()
        db.session.rollback()

        expected_res = ["tutu", "tata", "tete"]

        users = User.query.all()
        for i, user in enumerate(users):
            assert user.pseudo == expected_res[i]

        db.session.close()

    def test_verify_password(self, client):
        """Test the checking of password"""
        pwds = ["test", "Test", "123"]
        expected_res = [True, False, False]

        titi = User.query.filter_by(pseudo='tutu').first()

        for i, pwd in enumerate(pwds):
            assert titi.check_password(pwd) == expected_res[i]

        db.session.close()


class TestArticle:
    """Tests for the Article model"""
    def test_append_article(selfself, client):
        """test the insertion of a new Article"""
        titi = User(pseudo='tutu', email='titi@example.fr')
        titi.set_password('test')
        toto = User(pseudo='tata', email='toto@example.fr')
        toto.set_password('123')
        db.session.add(titi)
        db.session.add(toto)
        db.session.commit()

        article1 = Article(name='a1', price=5.2, description="my article n°1",
                           location='right here', author=titi)
        article2 = Article(name='a2', price=0,
                           location='just there', author=titi)
        article3 = Article(name='a3', price=300.0, description="my article n°3",
                           author=toto)
        db.session.add_all([article1, article2, article3])
        db.session.commit()

        titi_articles = User.query.filter_by(pseudo='tutu').first().articles.all()
        toto_articles = User.query.filter_by(pseudo='tata').first().articles.all()

        assert titi_articles == [article1, article2]
        assert toto_articles == [article3]

        article_no_name = Article(price=3.1415, description="my article with no name",
                                  author=titi)
        db.session.add(article_no_name)
        with pytest.raises(IntegrityError):
            db.session.commit()
        db.session.rollback()

        article_no_price = Article(name='article no price', author=titi)
        db.session.add(article_no_price)
        with pytest.raises(IntegrityError):
            db.session.commit()
        db.session.rollback()

        article_no_author = Article(name='article no author', price=399)
        db.session.add(article_no_author)
        with pytest.raises(IntegrityError):
            db.session.commit()
        db.session.rollback()

        db.session.close()


# TODO: Test the Photo model.
