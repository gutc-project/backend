"""tests for the signup/signin process"""

import json
import pytest
from datetime import datetime

from config import Config
from app.models import User
from tests.utils import get_mocked_client


class TestConfig(Config):
    """Testing configuration"""
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'


client = get_mocked_client(TestConfig)

# testing data
toto_data = {'pseudo': 'tata', 'email': 'toto@example.fr',
             'password': 'test'}

titi_data = {'pseudo': 'tutu', 'email': 'titi@example.fr',
             'password': 'test'}
# testing data

class TestAuth:
    """tests for the signup/signin process"""
    def test_signup(self, client):
        """Test the signup precess"""
        rv = client.post('/api/auth/signup', data=json.dumps(toto_data), content_type='application/json')
        assert rv.status_code == 201 and rv.get_json() == {'msg': 'user add'}

        rv = client.post('/api/auth/signup', data=json.dumps(titi_data), content_type='application/json')
        assert rv.status_code == 201 and rv.get_json() == {'msg': 'user add'}

        toto = User.query.filter_by(pseudo='tata').first()
        assert toto.pseudo == toto_data['pseudo'] and \
               toto.email == toto_data['email'] and \
               toto.last_connection.date() == datetime.utcnow().date()
        assert toto.check_password(toto_data['password'])
        assert not toto.check_password("123")

        titi = User.query.filter_by(pseudo='tutu').first()
        assert titi.pseudo == titi_data['pseudo'] and \
               titi.email == titi_data['email'] and \
               titi.last_connection.date() == datetime.utcnow().date()
        assert titi.check_password(titi_data['password'])
        assert not toto.check_password("123")

        tete_same_pseudo = {'pseudo': 'tata', 'email': 'tete@example.fr',
                            'password': 'test'}
        rv = client.post('/api/auth/signup', data=json.dumps(tete_same_pseudo), content_type='application/json')
        assert rv.status_code == 400 and rv.get_json() == {'msg': 'invalide pseudo'}

        tete_same_email = {'pseudo': 'tete', 'email': 'toto@example.fr',
                           'password': 'test'}
        rv = client.post('/api/auth/signup', data=json.dumps(tete_same_email), content_type='application/json')
        assert rv.status_code == 400 and rv.get_json() == {'msg': 'invalide email'}

        users = User.query.filter_by().all()
        assert len(users) == 2

    def test_isPseudoAvailable(self, client):
        """Test the auth/ispseudoavailable endpoint util"""
        rv = client.post('/api/auth/ispseudoavailable', data=json.dumps({'pseudo': 'tata'}),
                         content_type='application/json')
        assert rv.status_code == 200 and rv.get_json() == {'pseudo': 'already used'}

        rv = client.post('/api/auth/ispseudoavailable', data=json.dumps({'pseudo': 'toutou'}),
                         content_type='application/json')
        assert rv.status_code == 202 and rv.get_json() == {'pseudo': 'available'}

    def test_isEmailAvailable(selfself, client):
        """Test the auth/isemailavailable endpoint util"""
        rv = client.post('/api/auth/isemailavailable', data=json.dumps({'email': 'toto@example.fr'}),
                         content_type='application/json')
        assert rv.status_code == 200 and rv.get_json() == {'email': 'already used'}

        rv = client.post('/api/auth/isemailavailable', data=json.dumps({'email': 'toutou@example.fr'}),
                         content_type='application/json')
        assert rv.status_code == 202 and rv.get_json() == {'email': 'available'}

    def test_signin(self, client):
        """Test the signin precess"""
        rv = client.post('/api/auth/signin',
                         data=json.dumps({'pseudo': toto_data['pseudo'], 'password': toto_data['password']}),
                         content_type='application/json')
        assert rv.status_code == 200 and rv.get_json()['msg'] == 'login successful' and \
               rv.get_json()['access_token'] and rv.get_json()['refresh_token']

        rv_nok_pseudo = client.post('/api/auth/signin',
                                    data=json.dumps({'pseudo': 'tete', 'password': toto_data['password']}),
                                    content_type='application/json')
        assert rv_nok_pseudo.status_code == 400 and rv_nok_pseudo.get_json() == {'msg': 'invalide pseudo'}

        rv = client.post('/api/auth/signin',
                         data=json.dumps({'pseudo': toto_data['pseudo'], 'password': '123'}),
                         content_type='application/json')
        assert rv.status_code == 400 and rv.get_json() == {'msg': 'invalide password'}
