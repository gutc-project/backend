import os

import pytest

from config import Config
from tests.utils import get_mocked_client


class TestConfig(Config):
    """Testing configuration"""
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
    STATIC_SPA_FOLDER = os.path.dirname(__file__)


client = get_mocked_client(TestConfig)


class TestSPA:
    """tests for the signup/signin process"""
    def test_spa(self, client):
        rv = client.get('/')
        assert rv.status_code == 200

        rv = client.get('/prout')
        assert rv.status_code == 404

        rv = client.get('/favicon.ico')
        assert rv.status_code == 200
