"""tests for the jwt authentication"""

import json
import time
from io import BytesIO

import pytest
from datetime import timedelta
from flask_jwt_extended import decode_token

from app.models import User
from config import Config
from tests.utils import get_mocked_client


class TestConfig(Config):
    """Testing configuration"""
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(seconds=0.5)


client = get_mocked_client(TestConfig)

# testing data
toto_data = {'pseudo': 'tata', 'email': 'toto@example.fr',
             'password': 'test'}

def get_data():
    with open('tests/test_img.png', 'rb') as f:
        img = (BytesIO(f.read()), 'test_img.png')
    data = {"name": "my article n°1", "price": 3.1415,
            "description": "blblblb", "image": img}
    return data
# testing data


class TestJWTProtection:
    """tests for the jwt authentication"""
    def test_access_token(self, client):
        """Test the access token behaviors"""
        rv = client.post('/api/auth/signup', data=json.dumps(toto_data), content_type='application/json')
        assert rv.status_code == 201 and rv.get_json() == {'msg': 'user add'}

        rv_no_token = client.post('/api/article/upload', data=get_data())
        assert rv_no_token.status_code == 401 and rv_no_token.get_json() == {"msg": "Missing Authorization Header"}

        bad_headers = {'Authorization': "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJm"
                                    "cmVzaCI6dHJ1ZSwianRpIjoiZjhmNDlmMjUtNTQ4OS00NmRjLTkyOWUtZTU2Y2QxOGZ"
                                    "hNzRlIiwidXNlcl9jbGFpbXMiOnt9LCJuYmYiOjE0NzQ0NzQ3OTEsImlhdCI6MTQ3ND"
                                    "Q3NDc5MSwiaWRlbnRpdHkiOiJ0ZXN0IiwiZXhwIjoxNDc0NDc1NjkxLCJ0eXBlIjoiY"
                                    "WNjZXNzIn0.vCy0Sec61i9prcGIRRCbG8e9NV6_wFH2ICFgUGCLKpc"}
        rv_bad_token = client.post('/api/article/upload', data=get_data(), headers=bad_headers)
        assert rv_bad_token.status_code == 422 and rv_bad_token.get_json() == {"msg": "Signature verification failed"}

        rv = client.post('/api/auth/signin', data=json.dumps({'pseudo': toto_data['pseudo'], 'password': toto_data['password']}),
                    content_type='application/json')
        access_token = rv.get_json()['access_token']

        rv = client.post('/api/article/upload', data=get_data(), headers={'Authorization': f"Bearer {access_token}"})
        assert rv.status_code == 200 and rv.get_json()['msg'] == 'new article upload'

        time.sleep(1.5)

        rv_access_expired = client.post('/api/article/upload', data=get_data(), headers={'Authorization': f"Bearer {access_token}"})
        assert rv_access_expired.status_code == 401 and rv_access_expired.get_json() == {'msg': 'Token has expired'}

    def test_refresh_token(self, client):
        """test the refresh token behaviors"""
        bad_headers = {'Authorization': "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJm"
                                    "cmVzaCI6dHJ1ZSwianRpIjoiZjhmNDlmMjUtNTQ4OS00NmRjLTkyOWUtZTU2Y2QxOGZ"
                                    "hNzRlIiwidXNlcl9jbGFpbXMiOnt9LCJuYmYiOjE0NzQ0NzQ3OTEsImlhdCI6MTQ3ND"
                                    "Q3NDc5MSwiaWRlbnRpdHkiOiJ0ZXN0IiwiZXhwIjoxNDc0NDc1NjkxLCJ0eXBlIjoiY"
                                    "WNjZXNzIn0.vCy0Sec61i9prcGIRRCbG8e9NV6_wFH2ICFgUGCLKpc"}
        rv_bad_token = client.post('/api/auth/refresh', headers=bad_headers)
        assert rv_bad_token.status_code == 422 and rv_bad_token.get_json() == {"msg": "Signature verification failed"}

        rv = client.post('/api/auth/signin', data=json.dumps({'pseudo': toto_data['pseudo'], 'password': toto_data['password']}),
                        content_type='application/json')
        refresh_token = rv.get_json()['refresh_token']
        access_token = rv.get_json()['access_token']

        rv = client.post('/api/auth/refresh', headers={'Authorization': f"Bearer {refresh_token}"})

        new_access_token = rv.get_json()["access_token"]

        assert access_token != new_access_token
        assert rv.status_code == 200 and new_access_token

        rv = client.post('/api/article/upload', data=get_data(), headers={'Authorization': f"Bearer {new_access_token}"})
        assert rv.status_code == 200 and rv.get_json()['msg'] == 'new article upload'

    def test_token_whitelisting(self, client):
        """Test the whitelisting of tokens"""
        rv = client.post('/api/auth/signin',
                         data=json.dumps({'pseudo': toto_data['pseudo'], 'password': toto_data['password']}),
                         content_type='application/json')
        access_jti = decode_token(rv.get_json()['access_token'])['jti']
        refresh_jti = decode_token(rv.get_json()['refresh_token'])['jti']
        toto = User.query.filter_by(pseudo=toto_data['pseudo']).first()
        assert toto.access_token == access_jti and toto.refresh_token == refresh_jti

        rv = client.post('/api/auth/signin',
                         data=json.dumps({'pseudo': toto_data['pseudo'], 'password': toto_data['password']}),
                         content_type='application/json')
        new_access_jti = decode_token(rv.get_json()['access_token'])['jti']
        new_refresh_jti = decode_token(rv.get_json()['refresh_token'])['jti']

        assert access_jti != new_access_jti and refresh_jti != new_refresh_jti
        toto = User.query.filter_by(pseudo=toto_data['pseudo']).first()
        assert toto.access_token == new_access_jti and toto.refresh_token == new_refresh_jti
